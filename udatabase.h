#ifndef UDATABASE_H
#define UDATABASE_H

#include <QtWidgets>
#include <QtSql>

enum { PRIVATE_QUERY, PUBLIC_QUERY };

enum { READ_QUERY, APPEND_QUERY, EDIT_QUERY, DELETE_QUERY, CREATE_TABLE, READ_DISTINCT, DROP_TABLE };

class DBOperations : public QObject
{
    Q_OBJECT

public:
	explicit DBOperations(QObject *parent = 0);
	~DBOperations();
	bool dbConnLocal(const QString sDBName, const QString sConnectionName);
	void dbCloseConn(const QString sConnectionName);
    bool dbWriteData(bool bIsNew, QSqlTableModel &model, const int iRow, const QStringList stlFields, const QList<QVariant> stlValues);
	bool dbRemoveData(QSqlTableModel &model, const int iRow);
	void dbQuery(const int querymode, const QString &sConnectionName, const int iAccessType, const QString &sFields, const QString &sValues, const QString &sTable, const bool &bSelect, const QString &sSelectString);

    QSqlError dbConnectionError;
	QSqlError dbLocalConnectionError;
	QSqlQuery query;
    QSqlTableModel *mdlMain;
    QSqlTableModel *mdlSourcesModel;

public slots:
    void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
    void TableAssign(QSqlTableModel &model, QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
    void setMdlFilter(QSqlTableModel &model,QString sFilter);
	void createLocalDB();

private:
	QSqlQueryModel *sqmTempModel;
    
	bool bFilterByUser;
	bool bFilter;
};

#endif // UDATABASE_H
