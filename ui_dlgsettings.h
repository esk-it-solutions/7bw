/********************************************************************************
** Form generated from reading UI file 'dlgsettings.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGS_H
#define UI_DLGSETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dlgSettings
{
public:
    QVBoxLayout *verticalLayout_4;
    QWidget *wgtTitleBar;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_4;
    QLabel *lblTitle;
    QSpacerItem *horizontalSpacer_5;
    QWidget *wgtContent;
    QVBoxLayout *verticalLayout_2;
    QWidget *wgtProfile;
    QHBoxLayout *horizontalLayout_5;
    QGroupBox *gbProfile;
    QHBoxLayout *horizontalLayout_4;
    QComboBox *cbProfiles;
    QPushButton *pbAddProfile;
    QPushButton *pbEditProfile;
    QPushButton *pbDeleteProfile;
    QSpacerItem *horizontalSpacer_2;
    QGroupBox *gbProfileSettings;
    QGridLayout *gridLayout;
    QGroupBox *gbDestination;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *leDestination;
    QPushButton *pbDestinationBrowse;
    QGroupBox *gbNumOfFullCopies;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *leNumOfFullCopies;
    QGroupBox *gbRootpath;
    QHBoxLayout *horizontalLayout_7;
    QLineEdit *leRootpath;
    QPushButton *pbRootpathBrowse;
    QGroupBox *gbSliceSize;
    QVBoxLayout *verticalLayout_6;
    QLineEdit *leSlicesize;
    QCheckBox *chbExclude;
    QGroupBox *gbExclude;
    QVBoxLayout *verticalLayout_3;
    QLineEdit *leExclude;
    QSpacerItem *verticalSpacer_2;
    QWidget *wgtSourcesControls;
    QGridLayout *gridLayout_4;
    QSpacerItem *verticalSpacer;
    QPushButton *pbAddSourceFl;
    QPushButton *pbRemoveSource;
    QPushButton *pbAddSourceFold;
    QGroupBox *gbSources;
    QVBoxLayout *verticalLayout;
    QListView *lvSources;
    QWidget *wgtControls;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer;
    QPushButton *pbDiscard;
    QPushButton *pbSave;

    void setupUi(QDialog *dlgSettings)
    {
        if (dlgSettings->objectName().isEmpty())
            dlgSettings->setObjectName(QString::fromUtf8("dlgSettings"));
        dlgSettings->resize(688, 606);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/clock.png"), QSize(), QIcon::Normal, QIcon::Off);
        dlgSettings->setWindowIcon(icon);
        dlgSettings->setStyleSheet(QString::fromUtf8("* {\n"
"background-color: #212121;\n"
"color: #7f7f7f;\n"
"}\n"
"QPushButton {\n"
"border-radius: 4px;\n"
"background-color: #4f4f53;\n"
"}\n"
"QPushButton:hover {\n"
"border-radius: 4px;\n"
"background-color: #595b5d;\n"
"}\n"
"QLineEdit {\n"
"border: none;\n"
"border-bottom: 2px solid qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 #747474, stop:1 #262626);\n"
"\n"
"}\n"
"QLineEdit:focus {\n"
"border-bottom: 2px solid qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 #43d7ff, stop:1 #3fc3fd);\n"
"}\n"
"QComboBox {\n"
"border: none;\n"
"border-bottom: 2px solid qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 #747474, stop:1 #262626);\n"
"\n"
"}\n"
"QComboBox:focus {\n"
"border: none;\n"
"border-bottom: 2px solid qlineargradient(x1:0, y1:0, x2:0, y2:1,\n"
"                stop:0 #43d7ff, stop:1 #3fc3fd);\n"
"}\n"
"QComboBox:on { \n"
"    padding-top: 3px;\n"
"    padding-left: 4px;\n"
"}\n"
"QComboBox::drop-down {\n"
"    subcontrol-origin: padding;\n"
"  "
                        "  subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border: none;\n"
"}\n"
"\n"
"QComboBox::down-arrow {\n"
"    image: url(:/icons/expand16-wh.png);\n"
"}\n"
"\n"
"QComboBox::down-arrow:on {\n"
"    top: 1px;\n"
"    left: 1px;\n"
"}"));
        verticalLayout_4 = new QVBoxLayout(dlgSettings);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        wgtTitleBar = new QWidget(dlgSettings);
        wgtTitleBar->setObjectName(QString::fromUtf8("wgtTitleBar"));
        wgtTitleBar->setMinimumSize(QSize(0, 40));
        wgtTitleBar->setMaximumSize(QSize(16777215, 40));
        wgtTitleBar->setStyleSheet(QString::fromUtf8("background-color: #37474f;\n"
"color: #e9ebec;"));
        horizontalLayout = new QHBoxLayout(wgtTitleBar);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 3, 0, 0);
        horizontalSpacer_4 = new QSpacerItem(10, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        lblTitle = new QLabel(wgtTitleBar);
        lblTitle->setObjectName(QString::fromUtf8("lblTitle"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        lblTitle->setFont(font);

        horizontalLayout->addWidget(lblTitle);

        horizontalSpacer_5 = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);


        verticalLayout_4->addWidget(wgtTitleBar);

        wgtContent = new QWidget(dlgSettings);
        wgtContent->setObjectName(QString::fromUtf8("wgtContent"));
        verticalLayout_2 = new QVBoxLayout(wgtContent);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        wgtProfile = new QWidget(wgtContent);
        wgtProfile->setObjectName(QString::fromUtf8("wgtProfile"));
        horizontalLayout_5 = new QHBoxLayout(wgtProfile);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        gbProfile = new QGroupBox(wgtProfile);
        gbProfile->setObjectName(QString::fromUtf8("gbProfile"));
        gbProfile->setMaximumSize(QSize(400, 16777215));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        gbProfile->setFont(font1);
        horizontalLayout_4 = new QHBoxLayout(gbProfile);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        cbProfiles = new QComboBox(gbProfile);
        cbProfiles->setObjectName(QString::fromUtf8("cbProfiles"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(cbProfiles->sizePolicy().hasHeightForWidth());
        cbProfiles->setSizePolicy(sizePolicy);
        cbProfiles->setMinimumSize(QSize(0, 36));
        cbProfiles->setMaximumSize(QSize(16777215, 36));
        QFont font2;
        font2.setPointSize(10);
        cbProfiles->setFont(font2);
        cbProfiles->setFrame(false);

        horizontalLayout_4->addWidget(cbProfiles);

        pbAddProfile = new QPushButton(gbProfile);
        pbAddProfile->setObjectName(QString::fromUtf8("pbAddProfile"));
        pbAddProfile->setMinimumSize(QSize(26, 26));
        pbAddProfile->setMaximumSize(QSize(26, 26));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/add24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbAddProfile->setIcon(icon1);

        horizontalLayout_4->addWidget(pbAddProfile);

        pbEditProfile = new QPushButton(gbProfile);
        pbEditProfile->setObjectName(QString::fromUtf8("pbEditProfile"));
        pbEditProfile->setMinimumSize(QSize(26, 26));
        pbEditProfile->setMaximumSize(QSize(26, 26));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/edit24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbEditProfile->setIcon(icon2);

        horizontalLayout_4->addWidget(pbEditProfile);

        pbDeleteProfile = new QPushButton(gbProfile);
        pbDeleteProfile->setObjectName(QString::fromUtf8("pbDeleteProfile"));
        pbDeleteProfile->setMinimumSize(QSize(26, 26));
        pbDeleteProfile->setMaximumSize(QSize(26, 26));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/remove24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbDeleteProfile->setIcon(icon3);

        horizontalLayout_4->addWidget(pbDeleteProfile);


        horizontalLayout_5->addWidget(gbProfile);

        horizontalSpacer_2 = new QSpacerItem(320, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);


        verticalLayout_2->addWidget(wgtProfile);

        gbProfileSettings = new QGroupBox(wgtContent);
        gbProfileSettings->setObjectName(QString::fromUtf8("gbProfileSettings"));
        gbProfileSettings->setFont(font1);
        gridLayout = new QGridLayout(gbProfileSettings);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gbDestination = new QGroupBox(gbProfileSettings);
        gbDestination->setObjectName(QString::fromUtf8("gbDestination"));
        gbDestination->setMinimumSize(QSize(282, 0));
        gbDestination->setFont(font1);
        horizontalLayout_3 = new QHBoxLayout(gbDestination);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        leDestination = new QLineEdit(gbDestination);
        leDestination->setObjectName(QString::fromUtf8("leDestination"));
        leDestination->setMinimumSize(QSize(0, 36));
        leDestination->setMaximumSize(QSize(16777215, 36));
        leDestination->setFont(font2);

        horizontalLayout_3->addWidget(leDestination);

        pbDestinationBrowse = new QPushButton(gbDestination);
        pbDestinationBrowse->setObjectName(QString::fromUtf8("pbDestinationBrowse"));
        pbDestinationBrowse->setMinimumSize(QSize(40, 32));
        pbDestinationBrowse->setMaximumSize(QSize(40, 32));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/foldop24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbDestinationBrowse->setIcon(icon4);

        horizontalLayout_3->addWidget(pbDestinationBrowse);


        gridLayout->addWidget(gbDestination, 0, 0, 1, 2);

        gbNumOfFullCopies = new QGroupBox(gbProfileSettings);
        gbNumOfFullCopies->setObjectName(QString::fromUtf8("gbNumOfFullCopies"));
        gbNumOfFullCopies->setMaximumSize(QSize(200, 16777215));
        gbNumOfFullCopies->setFont(font1);
        verticalLayout_5 = new QVBoxLayout(gbNumOfFullCopies);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        leNumOfFullCopies = new QLineEdit(gbNumOfFullCopies);
        leNumOfFullCopies->setObjectName(QString::fromUtf8("leNumOfFullCopies"));
        leNumOfFullCopies->setMinimumSize(QSize(0, 36));
        leNumOfFullCopies->setMaximumSize(QSize(16777215, 36));
        leNumOfFullCopies->setFont(font2);
        leNumOfFullCopies->setInputMask(QString::fromUtf8("D00"));
        leNumOfFullCopies->setText(QString::fromUtf8("3"));
        leNumOfFullCopies->setMaxLength(3);

        verticalLayout_5->addWidget(leNumOfFullCopies);


        gridLayout->addWidget(gbNumOfFullCopies, 0, 2, 1, 1);

        gbRootpath = new QGroupBox(gbProfileSettings);
        gbRootpath->setObjectName(QString::fromUtf8("gbRootpath"));
        gbRootpath->setMinimumSize(QSize(282, 0));
        gbRootpath->setFont(font1);
        horizontalLayout_7 = new QHBoxLayout(gbRootpath);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        leRootpath = new QLineEdit(gbRootpath);
        leRootpath->setObjectName(QString::fromUtf8("leRootpath"));
        leRootpath->setMinimumSize(QSize(0, 36));
        leRootpath->setMaximumSize(QSize(16777215, 36));
        leRootpath->setFont(font2);

        horizontalLayout_7->addWidget(leRootpath);

        pbRootpathBrowse = new QPushButton(gbRootpath);
        pbRootpathBrowse->setObjectName(QString::fromUtf8("pbRootpathBrowse"));
        pbRootpathBrowse->setMinimumSize(QSize(40, 32));
        pbRootpathBrowse->setMaximumSize(QSize(40, 32));
        pbRootpathBrowse->setIcon(icon4);

        horizontalLayout_7->addWidget(pbRootpathBrowse);


        gridLayout->addWidget(gbRootpath, 1, 0, 1, 2);

        gbSliceSize = new QGroupBox(gbProfileSettings);
        gbSliceSize->setObjectName(QString::fromUtf8("gbSliceSize"));
        gbSliceSize->setMaximumSize(QSize(200, 16777215));
        gbSliceSize->setFont(font1);
        verticalLayout_6 = new QVBoxLayout(gbSliceSize);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        leSlicesize = new QLineEdit(gbSliceSize);
        leSlicesize->setObjectName(QString::fromUtf8("leSlicesize"));
        leSlicesize->setMinimumSize(QSize(0, 36));
        leSlicesize->setMaximumSize(QSize(16777215, 36));
        leSlicesize->setFont(font2);
        leSlicesize->setText(QString::fromUtf8("9G"));
        leSlicesize->setMaxLength(32767);

        verticalLayout_6->addWidget(leSlicesize);


        gridLayout->addWidget(gbSliceSize, 1, 2, 1, 1);

        chbExclude = new QCheckBox(gbProfileSettings);
        chbExclude->setObjectName(QString::fromUtf8("chbExclude"));
        chbExclude->setFont(font1);

        gridLayout->addWidget(chbExclude, 2, 0, 1, 1);

        gbExclude = new QGroupBox(gbProfileSettings);
        gbExclude->setObjectName(QString::fromUtf8("gbExclude"));
        gbExclude->setMinimumSize(QSize(300, 0));
        gbExclude->setFont(font1);
        verticalLayout_3 = new QVBoxLayout(gbExclude);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        leExclude = new QLineEdit(gbExclude);
        leExclude->setObjectName(QString::fromUtf8("leExclude"));
        leExclude->setMinimumSize(QSize(0, 36));
        leExclude->setMaximumSize(QSize(16777215, 36));
        leExclude->setFont(font2);

        verticalLayout_3->addWidget(leExclude);


        gridLayout->addWidget(gbExclude, 3, 0, 1, 3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 4, 1, 1, 1);


        verticalLayout_2->addWidget(gbProfileSettings);

        wgtSourcesControls = new QWidget(wgtContent);
        wgtSourcesControls->setObjectName(QString::fromUtf8("wgtSourcesControls"));
        wgtSourcesControls->setMinimumSize(QSize(0, 0));
        gridLayout_4 = new QGridLayout(wgtSourcesControls);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, -1, 0);
        verticalSpacer = new QSpacerItem(17, 318, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 5, 6, 1, 1);

        pbAddSourceFl = new QPushButton(wgtSourcesControls);
        pbAddSourceFl->setObjectName(QString::fromUtf8("pbAddSourceFl"));
        pbAddSourceFl->setMinimumSize(QSize(40, 32));
        pbAddSourceFl->setMaximumSize(QSize(40, 32));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/add24p-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbAddSourceFl->setIcon(icon5);

        gridLayout_4->addWidget(pbAddSourceFl, 2, 6, 1, 1);

        pbRemoveSource = new QPushButton(wgtSourcesControls);
        pbRemoveSource->setObjectName(QString::fromUtf8("pbRemoveSource"));
        pbRemoveSource->setMinimumSize(QSize(40, 32));
        pbRemoveSource->setMaximumSize(QSize(40, 32));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/icons/remove24p-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbRemoveSource->setIcon(icon6);

        gridLayout_4->addWidget(pbRemoveSource, 4, 6, 1, 1);

        pbAddSourceFold = new QPushButton(wgtSourcesControls);
        pbAddSourceFold->setObjectName(QString::fromUtf8("pbAddSourceFold"));
        pbAddSourceFold->setMinimumSize(QSize(40, 32));
        pbAddSourceFold->setMaximumSize(QSize(40, 32));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/icons/addfold24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbAddSourceFold->setIcon(icon7);
        pbAddSourceFold->setIconSize(QSize(16, 16));

        gridLayout_4->addWidget(pbAddSourceFold, 0, 6, 2, 1);

        gbSources = new QGroupBox(wgtSourcesControls);
        gbSources->setObjectName(QString::fromUtf8("gbSources"));
        gbSources->setFont(font1);
        verticalLayout = new QVBoxLayout(gbSources);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lvSources = new QListView(gbSources);
        lvSources->setObjectName(QString::fromUtf8("lvSources"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font3.setPointSize(10);
        lvSources->setFont(font3);

        verticalLayout->addWidget(lvSources);


        gridLayout_4->addWidget(gbSources, 0, 4, 6, 1);


        verticalLayout_2->addWidget(wgtSourcesControls);


        verticalLayout_4->addWidget(wgtContent);

        wgtControls = new QWidget(dlgSettings);
        wgtControls->setObjectName(QString::fromUtf8("wgtControls"));
        wgtControls->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"border-radius: 4px;\n"
"background-color: #4f4f53;\n"
"padding:9px;\n"
"}\n"
"QPushButton:hover {\n"
"border-radius: 4px;\n"
"background-color: #595b5d;\n"
"}"));
        horizontalLayout_6 = new QHBoxLayout(wgtControls);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer = new QSpacerItem(652, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer);

        pbDiscard = new QPushButton(wgtControls);
        pbDiscard->setObjectName(QString::fromUtf8("pbDiscard"));
        pbDiscard->setMinimumSize(QSize(0, 32));
        pbDiscard->setMaximumSize(QSize(16777215, 32));
        pbDiscard->setFont(font1);
        pbDiscard->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"background-color:#ff5353;\n"
"color: #f1f1f1;\n"
"}\n"
"QPushButton:hover{\n"
"background-color:#d44747;\n"
"}"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/icons/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbDiscard->setIcon(icon8);
        pbDiscard->setIconSize(QSize(22, 22));
        pbDiscard->setFlat(true);

        horizontalLayout_6->addWidget(pbDiscard);

        pbSave = new QPushButton(wgtControls);
        pbSave->setObjectName(QString::fromUtf8("pbSave"));
        pbSave->setMinimumSize(QSize(0, 32));
        pbSave->setMaximumSize(QSize(16777215, 32));
        pbSave->setFont(font1);
        pbSave->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"background-color:#6200ee;\n"
"color: #f1f1f1;\n"
"}\n"
"QPushButton:hover{\n"
"background-color:#5a02d2;\n"
"}"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/icons/save24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbSave->setIcon(icon9);
        pbSave->setIconSize(QSize(16, 22));

        horizontalLayout_6->addWidget(pbSave);


        verticalLayout_4->addWidget(wgtControls);

        QWidget::setTabOrder(cbProfiles, pbAddProfile);
        QWidget::setTabOrder(pbAddProfile, pbEditProfile);
        QWidget::setTabOrder(pbEditProfile, pbDeleteProfile);
        QWidget::setTabOrder(pbDeleteProfile, leDestination);
        QWidget::setTabOrder(leDestination, pbDestinationBrowse);
        QWidget::setTabOrder(pbDestinationBrowse, leNumOfFullCopies);
        QWidget::setTabOrder(leNumOfFullCopies, leRootpath);
        QWidget::setTabOrder(leRootpath, pbRootpathBrowse);
        QWidget::setTabOrder(pbRootpathBrowse, leSlicesize);
        QWidget::setTabOrder(leSlicesize, chbExclude);
        QWidget::setTabOrder(chbExclude, leExclude);
        QWidget::setTabOrder(leExclude, lvSources);
        QWidget::setTabOrder(lvSources, pbAddSourceFold);
        QWidget::setTabOrder(pbAddSourceFold, pbAddSourceFl);
        QWidget::setTabOrder(pbAddSourceFl, pbRemoveSource);
        QWidget::setTabOrder(pbRemoveSource, pbSave);
        QWidget::setTabOrder(pbSave, pbDiscard);

        retranslateUi(dlgSettings);
        QObject::connect(chbExclude, SIGNAL(toggled(bool)), gbExclude, SLOT(setVisible(bool)));

        QMetaObject::connectSlotsByName(dlgSettings);
    } // setupUi

    void retranslateUi(QDialog *dlgSettings)
    {
        dlgSettings->setWindowTitle(QCoreApplication::translate("dlgSettings", "7BW Settings", nullptr));
        lblTitle->setText(QCoreApplication::translate("dlgSettings", "BW - Settings", nullptr));
        gbProfile->setTitle(QCoreApplication::translate("dlgSettings", "Profile", nullptr));
#if QT_CONFIG(tooltip)
        pbAddProfile->setToolTip(QCoreApplication::translate("dlgSettings", "<html><head/><body><p>Create new profile</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        pbEditProfile->setToolTip(QCoreApplication::translate("dlgSettings", "<html><head/><body><p>Change profile name</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        pbEditProfile->setText(QString());
#if QT_CONFIG(tooltip)
        pbDeleteProfile->setToolTip(QCoreApplication::translate("dlgSettings", "<html><head/><body><p>Delete profile</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        gbProfileSettings->setTitle(QCoreApplication::translate("dlgSettings", "Profile settings", nullptr));
        gbDestination->setTitle(QCoreApplication::translate("dlgSettings", "Destination path", nullptr));
        pbDestinationBrowse->setText(QString());
        gbNumOfFullCopies->setTitle(QCoreApplication::translate("dlgSettings", "Number of full copies", nullptr));
        gbRootpath->setTitle(QCoreApplication::translate("dlgSettings", "Root path", nullptr));
        pbRootpathBrowse->setText(QString());
        gbSliceSize->setTitle(QCoreApplication::translate("dlgSettings", "Slicesize", nullptr));
        chbExclude->setText(QCoreApplication::translate("dlgSettings", "Add exclutions", nullptr));
        gbExclude->setTitle(QCoreApplication::translate("dlgSettings", "Excluding extentions", nullptr));
#if QT_CONFIG(tooltip)
        pbAddSourceFl->setToolTip(QCoreApplication::translate("dlgSettings", "<html><head/><body><p>Add source file</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        pbRemoveSource->setToolTip(QCoreApplication::translate("dlgSettings", "<html><head/><body><p>Delete source</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        pbAddSourceFold->setToolTip(QCoreApplication::translate("dlgSettings", "<html><head/><body><p>Add source folder</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        gbSources->setTitle(QCoreApplication::translate("dlgSettings", "Sources", nullptr));
        pbDiscard->setText(QCoreApplication::translate("dlgSettings", "DISCARD", nullptr));
        pbSave->setText(QCoreApplication::translate("dlgSettings", "SAVE", nullptr));
    } // retranslateUi

};

namespace Ui {
    class dlgSettings: public Ui_dlgSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGSETTINGS_H
