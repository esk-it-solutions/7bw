/**************************************************************************
**
** Copyright (c) 2018, esk it solutions
** wsite: https://esk-its.ru
**
** This file is part of Backup Wrapper application.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**

** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
** STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
** OF THE POSSIBILITY OF SUCH DAMAGE.
**
**************************************************************************
**
** Copyright (c) 2018, esk it solutions
** wsite: https://esk-its.ru
**
** Этот файл является частью приложения Backup Wrapper.
**
** Распространение и использование данного файла в виде исходного кода
** и в бинарном виде с или без модификации допускается, только
** при выполнении следующих положений:
**   * В исходном коде должны быть сохранены ссылки на правообладателя
**	   и последующее предупреждение.
**   * При распространении в бинарном виде ссылка на правообладателя
**     и последующее предупреждение должны быть указана в документации
**     и/или других материалах поставляемых в составе дистрибутива.
**
** ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПОСТАВЛЯЕТСЯ ПРАВООБЛАДАТЕЛЕМ
** И ПРЕДСТАВИТЕЛЯМИ ПРАВООБЛАДАТЕЛЯ "КАК ЕСТЬ" БЕЗ ПРЕДОСТАВЛЕНИЯ
** КАКИХ-ЛИБО ГАРАНТИЙ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЕЙ
** РАБОТОСПОСОБНОСТИ, КОММЕРЧЕСКОЙ ПРИБЫЛИ. ПРАВООБЛАДАТЕЛЬ
** И ЕГО ПРЕДСТАВИТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ЗА ЛЮБОЙ ПРЯМОЙ
** ИЛИ КОСВЕННЫЙ УЩЕРБ, ПРИЧИНЕННЫЙ ЛЮБЫМ ЛИЦАМ ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ
** ДАННОГО ПРОДУКТА (ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ПОВРЕЖДЕНИЕ ИНФОРМАЦИИ,
** НАРУШЕНИЕ РАБОТОСПОСОБНОСТИ ИНОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
** И ИНФОРМАЦИОННЫХ СИСТЕМ) ВНЕ ЗАВИСИМОСТИ ОТ ФАКТА КОРРЕКТНОГО
** ИЛИ НЕ КОРРЕКТНОГО ИСПОЛЬЗОВАНИЯ ДАННОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ.
**
**************************************************************************/

#include "wgtmode.h"
#include "ui_wgtmode.h"

wgtMode::wgtMode(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::wgtMode)
{
    ui->setupUi(this);
    QFont font = QFont("Roboto Regular", 10, 1);
    setFont(font);
    dSets=new dlgSettings(0);
    dSets->hide();
    connect(dSets, SIGNAL(showMode()), this, SLOT(showMode()));
    connect(this, SIGNAL(closeDB()), dSets, SLOT(closeDB()));
}

wgtMode::~wgtMode()
{
    delete ui;
}

void wgtMode::on_pbArchive_clicked()
{
    hide();
    dSets->show();
}

void wgtMode::showMode() {
    dSets->hide();
    show();
}

void wgtMode::on_pbRestore_clicked()
{
    QString strArcFile="";
    QString strDestinationPath="";
    QString strExec="";
    QFileInfo fliArcFileBase;
    QProcess *DArc = new QProcess;

    strArcFile=QFileDialog::getOpenFileName(this,tr("Select archive file"),"","Archives (*.dar)");
    strDestinationPath=QFileDialog::getExistingDirectory(this,tr("Select destination folder"),"",QFileDialog::ShowDirsOnly);

    if(strArcFile!="" && strDestinationPath!="") {
        fliArcFileBase.setFile(strArcFile);

        strExec+="dar -x ";
        strExec+=strDestinationPath; //set destination path
        strExec+="/ ";
        strExec+=fliArcFileBase.absolutePath()+"/"+fliArcFileBase.baseName();
        DArc->startDetached(strExec);
        DArc->waitForStarted(500000);
    }

    QMessageBox::information(0,"BW","Restoring files started");
}

void wgtMode::init() {
    dSets->strArcpath=strArcpath;
    dSets->init(bFirstRun);
}

void wgtMode::on_pbExit_clicked()
{
    emit closeDB();
    qApp->exit(0);
}