#include "udatabase.h"

DBOperations::DBOperations(QObject *parent)
	: QObject(parent) {
    sqmTempModel = new QSqlQueryModel();

    mdlMain=new QSqlTableModel();
    mdlSourcesModel=new QSqlTableModel();

	bFilterByUser = false;
	bFilter = false;
}

DBOperations::~DBOperations() {

}

bool DBOperations::dbConnLocal(const QString sDBName, const QString sConnectionName) {
	QString sDBNameString;
	sDBNameString=sDBName;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",sConnectionName);
    db.setDatabaseName(sDBNameString);
    if(!db.open()) {
		dbLocalConnectionError=db.lastError();
		return false;
	}
    return true;
}

void DBOperations::dbCloseConn(const QString sConnectionName) {
	QSqlDatabase sqlDB = QSqlDatabase::database(sConnectionName);
	if(sqlDB.isOpen()) sqlDB.close();
	sqlDB=QSqlDatabase();
	QSqlDatabase::removeDatabase(sConnectionName);
}

void DBOperations::dbQuery(const int querymode, const QString &sConnectionName, const int iAccessType, const QString &sFields, const QString &sValues, const QString &sTable, const bool &bSelect, const QString &sSelectString) {
    QSqlDatabase sqlDB = QSqlDatabase::database(sConnectionName);
	QSqlQuery sqlQueryRt(sqlDB);
	QString sQueryString;
    QStringList stlFieldsList;
    QStringList stlValuesList;
    int iIterator=0;
	
    switch(iAccessType) {
        case 0: //Read
            sQueryString = "SELECT " + sFields + " FROM " + sTable;
            if(bSelect) sQueryString+=" WHERE " + sSelectString;
            sQueryString += ";";
        break;
        case 1: //Append
            sQueryString = "INSERT INTO " + sTable + "(" + sFields + ") values(" + sValues + ");";
        break;
        case 2: //Edit
            sQueryString = "UPDATE " + sTable + " SET ";
            stlFieldsList = sFields.split("$");
            stlValuesList = sValues.split("$");

            while(iIterator<stlFieldsList.count()) {
                if(iIterator>0 && iIterator!=stlFieldsList.count()) sQueryString +=", ";
                sQueryString += stlFieldsList.at(iIterator) + "=";
                sQueryString += stlValuesList.at(iIterator);
                iIterator++;
            }
            if(bSelect) sQueryString+=" WHERE " + sSelectString + ";";
            else sQueryString+=";";
        break;
        case 3: //Delete
            sQueryString = "DELETE FROM " + sTable + " WHERE " + sSelectString;
            sQueryString += ";";
        break;
        case 4: //Create table
            sQueryString = "CREATE TABLE " + sTable + " (" + sFields + ")";
            sQueryString += ";";
        break;
		case 5: //Read distinct
			sQueryString = "SELECT DISTINCT " + sFields + " FROM " + sTable;
			if (bSelect) sQueryString += " WHERE " + sSelectString;
			sQueryString += ";";
		break;
        case 6: //Drop table
            sQueryString = "DROP " + sTable;
            sQueryString += ";";
    break;
    }
#ifdef DEBUG
	qDebug()<<sQueryString;
#endif
	if(querymode==PRIVATE_QUERY) sqlQueryRt.exec(sQueryString);
	else if(querymode==PUBLIC_QUERY) {
		query=sqlQueryRt;
		query.exec(sQueryString);
		sqlQueryRt.finish();
	}
	
	if(sqlQueryRt.lastError().isValid()) {
           QString qErrNum;
           qErrNum.setNum(sqlQueryRt.lastError().number());
           if(sqlQueryRt.lastError().number()!=1) {
                QMessageBox::critical(0, QObject::tr("BW"),QObject::tr("Error Number: ")+qErrNum+": "+sqlQueryRt.lastError().text(), QMessageBox::Ok);
                qApp->exit(171);
           }
    }

	sqlQueryRt.finish();
}

bool DBOperations::dbWriteData(bool bIsNew, QSqlTableModel &model, const int iRow, const QStringList stlFields, const QList<QVariant> stlValues) {
	if(bIsNew) model.insertRow(iRow);
			
	for(int i=0;i<stlFields.size();++i)
		model.setData(model.index(iRow, stlFields.at(i).toInt()), stlValues.at(i));
	
#ifdef DEBUG
    qDebug() << model.lastError().text();
#endif
	if (model.submitAll()) return true;
	else return false;
}

void DBOperations::ExecQuery(const int querymode,QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString) {
	dbQuery(querymode,sConnectionName, iAccessType, sFields, sValues, sTable, bSelect, sSelectString);
}

void DBOperations::TableAssign(QSqlTableModel &model, QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser) {
	bFilterByUser=(sFilterByUser!="");
	bFilter=(sFilter!="");

    model.setEditStrategy(qSqlTMES);
    model.setTable(sTableName);
    model.setFilter(sFilterByUser+sFilter);
    model.select();
}

void DBOperations::setMdlFilter(QSqlTableModel &model,QString sFilter) {
    model.setFilter(sFilter);
    model.select();
}

void DBOperations::createLocalDB() {
    dbQuery(PRIVATE_QUERY, "dbsettings", 4, "DBLocal boolean not null CHECK (DBLocal IN(0, 1)), DBServer varchar(300) null, DBName varchar(300) null, DBLogin varchar(300) null, DBPwd varchar(300) null, DBPath varchar(300) null, updatepath varchar(300) null, lastuser varchar(300) null", "", "main", false, "");
}

bool DBOperations::dbRemoveData(QSqlTableModel &model, const int iRow) {
	return model.removeRows(iRow, 1);
}
