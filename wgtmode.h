#ifndef WGTMODE_H
#define WGTMODE_H

#include <QDialog>
#include "dlgsettings.h"

namespace Ui {
class wgtMode;
}

class wgtMode : public QDialog
{
    Q_OBJECT

public:
    explicit wgtMode(QWidget *parent = 0);
    bool bFirstRun;
    int startProgram(QString strProfile, QString strArchType);
    void init();
    QString strArcpath;
    ~wgtMode();

public slots:
    void showMode();

signals:
    void closeDB();

private slots:
    void on_pbArchive_clicked();
    void on_pbRestore_clicked();
    void on_pbExit_clicked();

private:
    Ui::wgtMode *ui;
    dlgSettings *dSets;
};

#endif // WGTMODE_H
