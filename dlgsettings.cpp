﻿/**************************************************************************
**
** Copyright (c) 2018, esk it solutions
** wsite: https://esk-its.ru
**
** This file is part of Backup Wrapper application.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**

** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
** STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
** OF THE POSSIBILITY OF SUCH DAMAGE.
**
**************************************************************************
**
** Copyright (c) 2018, esk it solutions
** wsite: https://esk-its.ru
**
** Этот файл является частью приложения Backup Wrapper.
**
** Распространение и использование данного файла в виде исходного кода
** и в бинарном виде с или без модификации допускается, только
** при выполнении следующих положений:
**   * В исходном коде должны быть сохранены ссылки на правообладателя
**	   и последующее предупреждение.
**   * При распространении в бинарном виде ссылка на правообладателя
**     и последующее предупреждение должны быть указана в документации
**     и/или других материалах поставляемых в составе дистрибутива.
**
** ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПОСТАВЛЯЕТСЯ ПРАВООБЛАДАТЕЛЕМ
** И ПРЕДСТАВИТЕЛЯМИ ПРАВООБЛАДАТЕЛЯ "КАК ЕСТЬ" БЕЗ ПРЕДОСТАВЛЕНИЯ
** КАКИХ-ЛИБО ГАРАНТИЙ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЕЙ
** РАБОТОСПОСОБНОСТИ, КОММЕРЧЕСКОЙ ПРИБЫЛИ. ПРАВООБЛАДАТЕЛЬ
** И ЕГО ПРЕДСТАВИТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ЗА ЛЮБОЙ ПРЯМОЙ
** ИЛИ КОСВЕННЫЙ УЩЕРБ, ПРИЧИНЕННЫЙ ЛЮБЫМ ЛИЦАМ ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ
** ДАННОГО ПРОДУКТА (ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ПОВРЕЖДЕНИЕ ИНФОРМАЦИИ,
** НАРУШЕНИЕ РАБОТОСПОСОБНОСТИ ИНОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
** И ИНФОРМАЦИОННЫХ СИСТЕМ) ВНЕ ЗАВИСИМОСТИ ОТ ФАКТА КОРРЕКТНОГО
** ИЛИ НЕ КОРРЕКТНОГО ИСПОЛЬЗОВАНИЯ ДАННОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ.
**
**************************************************************************/

#include "dlgsettings.h"
#include "ui_dlgsettings.h"

dlgSettings::dlgSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgSettings)
{
    ui->setupUi(this);
    QFont font = QFont("Roboto Regular", 10, 1);
    setFont(font);
    ui->gbExclude->setVisible(ui->chbExclude->isChecked());
	setWindowFlags(Qt::FramelessWindowHint);
	bTBMousePressed=false;

    dboper=new DBOperations();
    connect(this,SIGNAL(TableAssign(QSqlTableModel &, QSqlTableModel::EditStrategy, QString, QString, QString)),dboper, SLOT(TableAssign(QSqlTableModel &, QSqlTableModel::EditStrategy, QString, QString, QString)));
}

dlgSettings::~dlgSettings()
{
    delete ui;
}

void dlgSettings::init(bool bFirstRun) {
    QStringList stlFields;
    QList<QVariant> stlValues;

    dboper->dbQuery(PUBLIC_QUERY,"ZBWDB",READ_QUERY,"*","","zbw_main",false,"");
	dboper->query.next();

    dboper->mdlMain = new QSqlTableModel(0, QSqlDatabase::database("ZBWDB"));
    dboper->mdlSourcesModel = new QSqlTableModel(0, QSqlDatabase::database("ZBWDB"));

    if(!dboper->query.isValid()) {
        if(bFirstRun) dboper->dbQuery(PRIVATE_QUERY, "ZBWDB",CREATE_TABLE,"id INTEGER PRIMARY KEY AUTOINCREMENT, profile varchar(150) not null, rootfolder varchar(200) not null, destpath varchar(200) not null, numofcopies INTEGER not null, protect bool not null, password varchar(200) null, exclude bool not null, exclutions varchar(200) null, slicesize varchar(200) not null","","zbw_main",false,"");

        /*
         * 0 id INTEGER PRIMARY KEY AUTOINCREMENT
         * 1 profile varchar(150) not null
         * 2 rootfolder varchar(200) not null
         * 3 destpath varchar(200) not null
         * 4 numofcopies INTEGER not null
         * 5 protect bool not null
         * 6 password varchar(200) null
         * 7 exclude bool not null
         * 8 exclutions varchar(200) null
         * 9 slicesize varchar(200) not null
        */

        emit TableAssign(*dboper->mdlMain, QSqlTableModel::OnRowChange, "zbw_main", "", "");
        stlFields.append("1"); //profile
        stlValues.append("default");
        stlFields.append("2"); //rootpath
        stlValues.append(QDir::homePath());
        stlFields.append("3"); //destpath
        stlValues.append(QDir::homePath()+"/BWTemp");
        stlFields.append("4"); //numofcopies
        stlValues.append(3);
        stlFields.append("5"); //protect
        stlValues.append(0);
        stlFields.append("7"); //exclude
        stlValues.append(0);
        stlFields.append("9"); //slicesize
        stlValues.append("9G");

        dboper->dbWriteData(true, *dboper->mdlMain, 0, stlFields, stlValues);
        dboper->mdlMain->select();
        dboper->mdlMain->setFilter("profile='default'");
        dboper->mdlMain->select();

        dboper->dbQuery(PRIVATE_QUERY, "ZBWDB", CREATE_TABLE,"sources varchar(200) not null","","zbw_"+dboper->mdlMain->record(0).value("id").toString(),false,"");

        emit TableAssign(*dboper->mdlSourcesModel, QSqlTableModel::OnRowChange, "zbw_"+dboper->mdlMain->record(0).value("id").toString(), "", "");
        stlFields.clear();
        stlValues.clear();
        stlFields.append("0"); //source
        stlValues.append(QDir::homePath());

        dboper->dbWriteData(true, *dboper->mdlSourcesModel, 0, stlFields, stlValues);

        loadSettings();
    }
    else loadSettings();
}

void dlgSettings::loadSettings() {
    QSqlQuery sqlMain;
    vReIndexComboBoxItems();
    ui->cbProfiles->setCurrentIndex(ui->cbProfiles->findText("default"));

    loadProfile("default");

    ui->pbEditProfile->setEnabled(false);
    ui->pbDeleteProfile->setEnabled(false);
}

void dlgSettings::on_pbDestinationBrowse_clicked()
{
    ui->leDestination->setText(QFileDialog::getExistingDirectory(this,tr("Select destination folder"),"",QFileDialog::ShowDirsOnly));
}

void dlgSettings::on_pbRootpathBrowse_clicked()
{
    ui->leRootpath->setText(QFileDialog::getExistingDirectory(this, tr("Select root folder"), "", QFileDialog::ShowDirsOnly));
}

void dlgSettings::on_pbAddSourceFold_clicked()
{
    QString sSrcFolder=QFileDialog::getExistingDirectory(this,tr("Add source folder"),"",QFileDialog::ShowDirsOnly);
    QStringList stlFields;
    QList<QVariant> stlValues;
    stlFields.append("0"); //source
    stlValues.append(sSrcFolder);
    if(dboper->mdlSourcesModel->rowCount()>0)
        while(dboper->mdlSourcesModel->canFetchMore()) dboper->mdlSourcesModel->fetchMore();

    dboper->dbWriteData(true,*dboper->mdlSourcesModel,dboper->mdlSourcesModel->rowCount(),stlFields,stlValues);

    dboper->mdlSourcesModel->select();
}

void dlgSettings::on_pbRemoveSource_clicked()
{
    dboper->dbRemoveData(*dboper->mdlSourcesModel,ui->lvSources->currentIndex().row());
    dboper->mdlSourcesModel->select();
}

void dlgSettings::on_pbAddSourceFl_clicked()
{
    QString sSrcFiles=QFileDialog::getOpenFileName(0,QObject::tr("Add source file"),"","*.*(*.*)");
    QStringList stlFields;
    QList<QVariant> stlValues;

    stlFields.append("0"); //source
    stlValues.append(sSrcFiles);

    if(dboper->mdlSourcesModel->rowCount()>0)
        while(dboper->mdlSourcesModel->canFetchMore()) dboper->mdlSourcesModel->fetchMore();

    dboper->dbWriteData(true,*dboper->mdlSourcesModel,dboper->mdlSourcesModel->rowCount(),stlFields,stlValues);

    dboper->mdlSourcesModel->select();
}

void dlgSettings::on_pbSave_clicked()
{
    //check fulfillness
    if ((ui->leRootpath->text() != "" && ui->leDestination->text() != "" && ui->leNumOfFullCopies->text() != "0" && ui->leSlicesize->text() != "") && (ui->chbExclude->isChecked() == false || ui->leExclude->text() != "")) {
        QString sTemp = "profile='";
        sTemp += ui->cbProfiles->itemText(ui->cbProfiles->currentIndex());
        sTemp += "'";
        QString sFields = "rootfolder$destpath$numofcopies";
        QString sValues = "'";
        sValues += ui->leRootpath->text();
        sValues += "'$'";
        sValues += ui->leDestination->text();
        sValues += "'$";
        sValues += ui->leNumOfFullCopies->text();

        if (ui->chbExclude->isChecked() == true && ui->leExclude->text() != "") {
            sFields += "$exclude$exclutions";
            sValues += "$1$'";
            sValues += ui->leExclude->text();
            sValues += "'";
        }
        else {
            sFields += "$exclude";
            sValues += "$0";
        }

        sFields += "$slicesize";
        sValues += "$'";
        sValues += ui->leSlicesize->text();
        sValues += "'";

        dboper->dbQuery(PRIVATE_QUERY, "ZBWDB", EDIT_QUERY, sFields, sValues, "zbw_main", true, sTemp);
    }
    else QMessageBox::information(0, tr("BW"), tr("Check required field."), QMessageBox::Ok);
}

void dlgSettings::on_pbAddProfile_clicked()
{
    QString strProfileName=QInputDialog::getText(this,tr("Enter new profile name"),tr("Profile name:"));
    QStringList stlFields;
    QList <QVariant> stlValues;
    if(ui->cbProfiles->findText(strProfileName,Qt::MatchExactly)==-1) {
        emit TableAssign(*dboper->mdlMain, QSqlTableModel::OnRowChange, "zbw_main", "", "");
        stlFields.append("1"); //profile
        stlValues.append(strProfileName);
        stlFields.append("2"); //rootpath
        stlValues.append(QDir::homePath());
        stlFields.append("3"); //destpath
        stlValues.append(QDir::homePath()+"/BWTemp");
        stlFields.append("4"); //numofcopies
        stlValues.append(3);
        stlFields.append("5"); //protect
        stlValues.append(0);
        stlFields.append("7"); //exclude
        stlValues.append(0);
        stlFields.append("9"); //slicesize
        stlValues.append("9G");

        while(dboper->mdlMain->canFetchMore()) dboper->mdlMain->fetchMore();

        dboper->dbWriteData(true, *dboper->mdlMain, dboper->mdlMain->rowCount(), stlFields, stlValues);
        dboper->mdlMain->select();
        dboper->mdlMain->setFilter("profile='"+strProfileName+"'");
        dboper->mdlMain->select();

        dboper->dbQuery(PRIVATE_QUERY, "ZBWDB", CREATE_TABLE,"sources varchar(200) not null","","zbw_"+dboper->mdlMain->record(0).value("id").toString(),false,"");
        emit TableAssign(*dboper->mdlSourcesModel, QSqlTableModel::OnRowChange, "zbw_"+dboper->mdlMain->record(0).value("id").toString(), "", "");
        stlFields.clear();
        stlValues.clear();
        stlFields.append("0"); //source
        stlValues.append(QDir::homePath());
        dboper->dbWriteData(true, *dboper->mdlSourcesModel, 0, stlFields, stlValues);

        vReIndexComboBoxItems();
        ui->cbProfiles->setCurrentIndex(ui->cbProfiles->findText(strProfileName,Qt::MatchExactly));

        loadProfile(strProfileName);
    }
}

void dlgSettings::on_pbEditProfile_clicked()
{
    if(ui->cbProfiles->currentText()!="default") {
        QString strProfileName=QInputDialog::getText(this,tr("Enter new profile name"),tr("Profile name:"));
        if(ui->cbProfiles->findText(strProfileName,Qt::MatchExactly)==-1) {
            QString strTemp="'"+strProfileName+"'";
            QString strTemp2="profile='"+ui->cbProfiles->currentText()+"'";
            dboper->dbQuery(PRIVATE_QUERY, "ZBWDB", EDIT_QUERY,"profile",strTemp,"zbw_main",true,strTemp2);
            vReIndexComboBoxItems();
            ui->cbProfiles->setCurrentIndex(ui->cbProfiles->findText(strProfileName,Qt::MatchExactly));
        }
    }
}

void dlgSettings::vReIndexComboBoxItems() {
    QStringList stlProfiles;
    ui->cbProfiles->clear();

    dboper->dbQuery(PUBLIC_QUERY, "ZBWDB", READ_QUERY,"profile","","zbw_main",false,"");

    while(dboper->query.next()) {
       stlProfiles.append(dboper->query.value("profile").toString());
    }

    ui->cbProfiles->addItems(stlProfiles);
}

void dlgSettings::on_pbDeleteProfile_clicked()
{
    if(ui->cbProfiles->currentText()!="default") {
        QString strDeletingProfileFilter="profile='"+ui->cbProfiles->currentText()+"'";
        dboper->dbQuery(PUBLIC_QUERY, "ZBWDB", READ_QUERY,"id,profile","","zbw_main",true,strDeletingProfileFilter);
		dboper->query.next();
        QString strDeletingIdFilter="id="+ dboper->query.value("id").toString();
        QString strDeletingTableName="zbw_"+ dboper->query.value("id").toString();
        
        ui->lvSources->setUpdatesEnabled(false);
        dboper->mdlSourcesModel->database().close();

		dboper->dbQuery(PRIVATE_QUERY, "ZBWDB", DELETE_QUERY,"","",strDeletingTableName,false,"");
        dboper->dbQuery(PRIVATE_QUERY, "ZBWDB", DROP_TABLE,"","",strDeletingTableName,false,"");
		dboper->dbQuery(PRIVATE_QUERY, "ZBWDB", DELETE_QUERY,"","","zbw_main",true,strDeletingIdFilter);

        loadProfile("default");

        vReIndexComboBoxItems();
        ui->cbProfiles->setCurrentIndex(ui->cbProfiles->findText("default"));
    }
}

void dlgSettings::on_cbProfiles_currentIndexChanged(const QString &arg1)
{
    loadProfile(arg1);
}

void dlgSettings::mousePressEvent(QMouseEvent* pe) {
	if(pe->pos().x()>=0 && pe->pos().x()<=621 && pe->pos().y()>=0 && pe->pos().y()<=25) {
		mptPosition=pe->pos();
		bTBMousePressed=true;
	}
	else bTBMousePressed=false;
}

void dlgSettings::mouseMoveEvent(QMouseEvent* pe) {
	if(bTBMousePressed) move(pe->globalPos() - mptPosition);
}

void dlgSettings::mouseReleaseEvent(QMouseEvent* pe) {
    bTBMousePressed=false;
}

void dlgSettings::loadProfile(QString profilename) {
    QString strFilter;
    strFilter="profile='"+profilename+"'";
    emit TableAssign(*dboper->mdlMain, QSqlTableModel::OnRowChange, "zbw_main", strFilter, "");

    ui->cbProfiles->setCurrentIndex(ui->cbProfiles->findText(profilename,Qt::MatchExactly));

    emit TableAssign(*dboper->mdlSourcesModel, QSqlTableModel::OnRowChange, "zbw_"+ dboper->mdlMain->record(0).value("id").toString(), "", "");
    ui->lvSources->setModel(dboper->mdlSourcesModel);
    ui->leRootpath->setText(dboper->mdlMain->record(0).value("rootfolder").toString());
    ui->leDestination->setText(dboper->mdlMain->record(0).value("destpath").toString());
    ui->leNumOfFullCopies->setText(dboper->mdlMain->record(0).value("numofcopies").toString());
    ui->chbExclude->setChecked(dboper->mdlMain->record(0).value("exclude").toBool());
    ui->gbExclude->setVisible(ui->chbExclude->isChecked());
    ui->leExclude->setText(dboper->mdlMain->record(0).value("exclutions").toString());
    ui->leSlicesize->setText(dboper->mdlMain->record(0).value("slicesize").toString());

    if(profilename!="default") {
        ui->pbEditProfile->setEnabled(true);
        ui->pbDeleteProfile->setEnabled(true);
    }
    else {
        ui->pbEditProfile->setEnabled(false);
        ui->pbDeleteProfile->setEnabled(false);
    }
}

void dlgSettings::closeDB() {
    dboper->query=QSqlQuery();
    dboper->mdlMain->clear();
    dboper->mdlSourcesModel->clear();
    dboper->dbCloseConn("");
}

void dlgSettings::closeEvent(QCloseEvent *event) {
    event->accept();
}

void dlgSettings::on_pbDiscard_clicked()
{
    loadProfile("default");

    vReIndexComboBoxItems();
    ui->cbProfiles->setCurrentIndex(ui->cbProfiles->findText("default"));
    emit showMode();
}

void dlgSettings::checkNumOfCopies(int iNumOfCopies,QDir dir,QFileInfoList dirContent) {
    QString strFilterListUpd="";
    QString strBaseNameUpd="";
    QFileInfoList dirContentUpd;
    QFile file;

    if(dirContent.length()>=iNumOfCopies) {
        for(int iFl=iNumOfCopies-1;iFl<dirContent.length();iFl++) {
            strBaseNameUpd=dirContent.value(iFl).baseName();
            strFilterListUpd=strBaseNameUpd+".?.dar";
            dirContentUpd=dir.entryInfoList(QStringList() << strFilterListUpd, QDir::Files | QDir::NoDotAndDotDot);

            for(int iFlUpd=0;iFlUpd<dirContentUpd.length();iFlUpd++) {
                file.setFileName(dirContentUpd.value(iFlUpd).absoluteFilePath());
                file.remove();
            }

            strFilterListUpd=strBaseNameUpd+"__\?\?\?\?-\?\?-\?\?_\?\?-\?\?-\?\?-upd.?.dar";
            dirContentUpd=dir.entryInfoList(QStringList() << strFilterListUpd, QDir::Files | QDir::NoDotAndDotDot);

            for(int iFlUpd=0;iFlUpd<dirContentUpd.length();iFlUpd++) {
                file.setFileName(dirContentUpd.value(iFlUpd).absoluteFilePath());
                file.remove();
            }
        }
    }
}
