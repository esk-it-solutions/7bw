<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>QObject</name>
    <message>
        <location filename="udatabase.cpp" line="100"/>
        <source>Error Number: </source>
        <translation>Номер ошибки: </translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="100"/>
        <source>BW</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="189"/>
        <source>Add source file</source>
        <translation>Добавить файл-источник</translation>
    </message>
</context>
<context>
    <name>dlgSettings</name>
    <message>
        <location filename="dlgsettings.ui" line="14"/>
        <source>7BW Settings</source>
        <translation>Настройки 7BW</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="251"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create new profile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Создать профиль&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="274"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Change profile name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Изменить имя профиля&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="300"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete profile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Удалить профиль&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="754"/>
        <source>Sources</source>
        <translation>Источники</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="731"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add source folder&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Добавить папку-источник&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="147"/>
        <source>BW - Settings</source>
        <translation>BW - Настройки</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="203"/>
        <source>Profile</source>
        <translation>Профиль</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="336"/>
        <source>Profile settings</source>
        <translation>Настройки профиля</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="354"/>
        <source>Destination path</source>
        <translation>Путь назначения</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="419"/>
        <source>Number of full copies</source>
        <translation>Количество полных копий</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="470"/>
        <source>Root path</source>
        <translation>Корень архивируемого каталога</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="535"/>
        <source>Slicesize</source>
        <translation>Размер части архива</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="833"/>
        <source>DISCARD</source>
        <translation>ОТКЛОНИТЬ</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="880"/>
        <source>SAVE</source>
        <translation>СОХРАНИТЬ</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="577"/>
        <source>Add exclutions</source>
        <translation>Добавить исключения</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="596"/>
        <source>Excluding extentions</source>
        <translation>Исключить расширения</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="685"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add source file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Добавить файл-источник&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlgsettings.ui" line="708"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete source&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Удалить источник&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="168"/>
        <source>Add source folder</source>
        <translation>Добавить папку-источник</translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="158"/>
        <source>Select destination folder</source>
        <translation>Выберите путь назначения</translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="163"/>
        <source>Select root folder</source>
        <translation>Выберите корневой каталог</translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="237"/>
        <source>BW</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="237"/>
        <source>Check required field.</source>
        <translation>Проверьте необходимые поля.</translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="242"/>
        <location filename="dlgsettings.cpp" line="287"/>
        <source>Enter new profile name</source>
        <translation>Введите имя нового профиля</translation>
    </message>
    <message>
        <location filename="dlgsettings.cpp" line="242"/>
        <location filename="dlgsettings.cpp" line="287"/>
        <source>Profile name:</source>
        <translation>Имя профиля:</translation>
    </message>
</context>
<context>
    <name>wgtMode</name>
    <message>
        <location filename="wgtmode.ui" line="20"/>
        <source>BW - Select mode</source>
        <translation>BW - Выбор режима</translation>
    </message>
    <message>
        <location filename="wgtmode.ui" line="52"/>
        <source>SELECT MODE</source>
        <translation>Выберите режим</translation>
    </message>
    <message>
        <location filename="wgtmode.ui" line="80"/>
        <source>ARCHIVE</source>
        <translation>АРХИВ</translation>
    </message>
    <message>
        <location filename="wgtmode.ui" line="113"/>
        <source>RESTORE (FULL)</source>
        <translation>ВОССТАНОВЛЕНИЕ (ПОЛНОЕ)</translation>
    </message>
    <message>
        <location filename="wgtmode.ui" line="149"/>
        <source>EXIT</source>
        <translation>ВЫХОД</translation>
    </message>
    <message>
        <location filename="wgtmode.cpp" line="100"/>
        <source>Select archive file</source>
        <translation>Выберите файл архива</translation>
    </message>
    <message>
        <location filename="wgtmode.cpp" line="101"/>
        <source>Select destination folder</source>
        <translation>Выберите путь назначения</translation>
    </message>
</context>
</TS>
