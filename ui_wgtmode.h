/********************************************************************************
** Form generated from reading UI file 'wgtmode.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WGTMODE_H
#define UI_WGTMODE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_wgtMode
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *sbSelMode;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbArchive;
    QPushButton *pbRestore;
    QPushButton *pbExit;

    void setupUi(QWidget *wgtMode)
    {
        if (wgtMode->objectName().isEmpty())
            wgtMode->setObjectName(QString::fromUtf8("wgtMode"));
        wgtMode->resize(606, 99);
        wgtMode->setMinimumSize(QSize(606, 99));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/clock.png"), QSize(), QIcon::Normal, QIcon::Off);
        wgtMode->setWindowIcon(icon);
        wgtMode->setStyleSheet(QString::fromUtf8("* {\n"
"background-color: #2d2d30;\n"
"color: #f1f1f1;\n"
"}\n"
"\n"
"QPushButton {\n"
"border-radius: 4px;\n"
"background-color: #4f4f53;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"border-radius: 4px;\n"
"background-color: #595b5d;\n"
"}"));
        verticalLayout_2 = new QVBoxLayout(wgtMode);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        sbSelMode = new QGroupBox(wgtMode);
        sbSelMode->setObjectName(QString::fromUtf8("sbSelMode"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        sbSelMode->setFont(font);
        horizontalLayout = new QHBoxLayout(sbSelMode);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pbArchive = new QPushButton(sbSelMode);
        pbArchive->setObjectName(QString::fromUtf8("pbArchive"));
        pbArchive->setMinimumSize(QSize(0, 32));
        pbArchive->setMaximumSize(QSize(16777215, 32));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        pbArchive->setFont(font1);
        pbArchive->setStyleSheet(QString::fromUtf8(""));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/archive24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbArchive->setIcon(icon1);
        pbArchive->setFlat(true);

        horizontalLayout->addWidget(pbArchive);

        pbRestore = new QPushButton(sbSelMode);
        pbRestore->setObjectName(QString::fromUtf8("pbRestore"));
        pbRestore->setMinimumSize(QSize(0, 32));
        pbRestore->setMaximumSize(QSize(16777215, 32));
        pbRestore->setFont(font1);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/unarchive24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbRestore->setIcon(icon2);
        pbRestore->setFlat(true);

        horizontalLayout->addWidget(pbRestore);

        pbExit = new QPushButton(sbSelMode);
        pbExit->setObjectName(QString::fromUtf8("pbExit"));
        pbExit->setMinimumSize(QSize(0, 32));
        pbExit->setFont(font1);
        pbExit->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"background-color:#ff5353;\n"
"color: #f1f1f1;\n"
"}\n"
"QPushButton:hover{\n"
"background-color:#d44747;\n"
"}"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/exit24-wh.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbExit->setIcon(icon3);

        horizontalLayout->addWidget(pbExit);


        verticalLayout_2->addWidget(sbSelMode);


        retranslateUi(wgtMode);

        QMetaObject::connectSlotsByName(wgtMode);
    } // setupUi

    void retranslateUi(QWidget *wgtMode)
    {
        wgtMode->setWindowTitle(QCoreApplication::translate("wgtMode", "BW - Select mode", nullptr));
        sbSelMode->setTitle(QCoreApplication::translate("wgtMode", "SELECT MODE", nullptr));
        pbArchive->setText(QCoreApplication::translate("wgtMode", "ARCHIVE", nullptr));
        pbRestore->setText(QCoreApplication::translate("wgtMode", "RESTORE (FULL)", nullptr));
        pbExit->setText(QCoreApplication::translate("wgtMode", "EXIT", nullptr));
    } // retranslateUi

};

namespace Ui {
    class wgtMode: public Ui_wgtMode {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WGTMODE_H
