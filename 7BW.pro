#-------------------------------------------------
#
# Project created by QtCreator 2013-04-01T22:08:07
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 7BW
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        dlgsettings.cpp \
    udatabase.cpp \
    wgtmode.cpp
	
TRANSLATIONS += \
    7bw_ru.ts	

HEADERS  += dlgsettings.h \
    udatabase.h \
    wgtmode.h

FORMS    += dlgsettings.ui \
    wgtmode.ui

RESOURCES += \
    icons.qrc
