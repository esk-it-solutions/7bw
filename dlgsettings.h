﻿/**************************************************************************
**
** Copyright (c) 2013, ezhik-s-koshkoy
** wsite: http://ezhik-s-koshkoy.ru
** 
** This file is part of 7zip Backup Wrapper.
** 
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**

** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
** STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
** OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************
**
** Copyright (c) 2013, ёжик с кошкой
** wsite: http://ezhik-s-koshkoy.ru
** 
** Этот файл является частью приложения 7zip Backup Wrapper.
** 
** Распространение и использование данного файла в виде исходного кода
** и в бинарном виде с или без модификации допускается, только
** при выполнении следующих положений:
**   * В исходном коде должны быть сохранены ссылки на правообладателя
**	   и последующее предупреждение.
**   * При распространении в бинарном виде ссылка на правообладателя
**     и последующее предупреждение должны быть указана в документации
**     и/или других материалах поставляемых в составе дистрибутива.
**
** ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПОСТАВЛЯЕТСЯ ПРАВООБЛАДАТЕЛЕМ
** И ПРЕДСТАВИТЕЛЯМИ ПРАВООБЛАДАТЕЛЯ "КАК ЕСТЬ" БЕЗ ПРЕДОСТАВЛЕНИЯ
** КАКИХ-ЛИБО ГАРАНТИЙ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЕЙ
** РАБОТОСПОСОБНОСТИ, КОММЕРЧЕСКОЙ ПРИБЫЛИ. ПРАВООБЛАДАТЕЛЬ
** И ЕГО ПРЕДСТАВИТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ЗА ЛЮБОЙ ПРЯМОЙ
** ИЛИ КОСВЕННЫЙ УЩЕРБ, ПРИЧИНЕННЫЙ ЛЮБЫМ ЛИЦАМ ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ
** ДАННОГО ПРОДУКТА (ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ПОВРЕЖДЕНИЕ ИНФОРМАЦИИ,
** НАРУШЕНИЕ РАБОТОСПОСОБНОСТИ ИНОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
** И ИНФОРМАЦИОННЫХ СИСТЕМ) ВНЕ ЗАВИСИМОСТИ ОТ ФАКТА КОРРЕКТНОГО
** ИЛИ НЕ КОРРЕКТНОГО ИСПОЛЬЗОВАНИЯ ДАННОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ. 
**************************************************************************/

#ifndef DLGSETTINGS_H
#define DLGSETTINGS_H

#include "udatabase.h"
#include <QDialog>

namespace Ui {
class dlgSettings;
}

class dlgSettings : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgSettings(QWidget *parent = 0);
    void init(bool bFirstRun);
    QString strArcpath;
    ~dlgSettings();
    
public slots:
    void loadSettings();
    void closeDB();

signals:
    void TableAssign(QSqlTableModel &model, QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
    void showMode();

private:
    Ui::dlgSettings *ui;
    bool bTBMousePressed;
	QPoint mptPosition;
    DBOperations *dboper;
    void loadProfile(QString profilename);
    void checkNumOfCopies(int iNumOfCopies,QDir dir,QFileInfoList dirContent);

private slots:
    void vReIndexComboBoxItems();
    void on_pbDestinationBrowse_clicked();
    void on_pbRootpathBrowse_clicked();
    void on_pbAddSourceFold_clicked();
    void on_pbRemoveSource_clicked();
    void on_pbAddSourceFl_clicked();
    void on_pbSave_clicked();
    void on_pbAddProfile_clicked();
    void on_pbEditProfile_clicked();
    void on_pbDeleteProfile_clicked();
    void on_cbProfiles_currentIndexChanged(const QString &arg1);
    void closeEvent(QCloseEvent *event);
    void on_pbDiscard_clicked();

protected:
	void mousePressEvent(QMouseEvent* pe);
	void mouseMoveEvent(QMouseEvent* pe);
	void mouseReleaseEvent(QMouseEvent* pe);
};

#endif // DLGSETTINGS_H
